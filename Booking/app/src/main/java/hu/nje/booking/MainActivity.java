package hu.nje.booking;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    private TextView destinationEditText;
    private Button checkInButton;
    private Button checkOutButton;
    private Spinner adultsSpinner;
    private Spinner childrenSpinner;
    private Spinner roomsSpinner;
    private CheckBox businessCheckBox;
    private Button searchButton;
    private TextView bookingsTextView;

    private boolean isCheckInSelected = true;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private List<Booking> bookings = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        destinationEditText = findViewById(R.id.destinationEditText);
        checkInButton = findViewById(R.id.checkInButton);
        checkOutButton = findViewById(R.id.checkOutButton);
        adultsSpinner = findViewById(R.id.adultsSpinner);
        childrenSpinner = findViewById(R.id.childrenSpinner);
        roomsSpinner = findViewById(R.id.roomsSpinner);
        businessCheckBox = findViewById(R.id.businessCheckBox);
        searchButton = findViewById(R.id.searchButton);
        bookingsTextView = findViewById(R.id.bookingsTextView);

        searchButton.setOnClickListener(v -> {
            // űrlapadatok összegyűjtése
            String destination = destinationEditText.getText().toString();
            if(destination.length() == 0) {
                showMessage("Úti cél kitöltése kötelező");
                return;
            }
            if(checkInDate == null || checkOutDate == null) {
                showMessage("Bejelentkezés és kijelentkezés dátumok kitöltése kötelező");
                return;
            }
            // kijelentkezés és bejelentkezés dátumok között eltelt napok száma
            long diffDays = Duration.between(checkInDate.atStartOfDay(), checkOutDate.atStartOfDay()).toDays();
            if(diffDays < 1) {
                showMessage("Kijelentkezés dátuma későbbi dátum legyen mint a bejelentkezésé");
            }

            String adultsStr = adultsSpinner.getSelectedItem().toString();
            int adults = Integer.parseInt(adultsStr);
            String childrenStr = childrenSpinner.getSelectedItem().toString();
            int children = Integer.parseInt(childrenStr);
            String roomsStr = roomsSpinner.getSelectedItem().toString();
            int rooms = Integer.parseInt(roomsStr);
            boolean isBusiness = businessCheckBox.isChecked();

            Booking booking = new Booking(destination, checkInDate, checkOutDate, adults, children, rooms, isBusiness);
            bookings.add(booking);
            bookingsTextView.setText("Foglalások száma: " + bookings.size() + "db");
        });

        initSpinner(R.array.number_of_adults, adultsSpinner);
        initSpinner(R.array.number_of_children, childrenSpinner);
        initSpinner(R.array.number_of_rooms, roomsSpinner);

        checkInButton.setOnClickListener(v -> {
            isCheckInSelected = true;

            LocalDate now = LocalDate.now();
            int year = now.getYear();
            int month = now.getMonthValue() - 1;
            int day = now.getDayOfMonth();

            DatePickerFragment fragment = DatePickerFragment.newInstance(year, month, day);
            fragment.show(getSupportFragmentManager(), "checkIn");
        });

        checkOutButton.setOnClickListener(v -> {
            if(checkInDate == null) {
                showMessage("Válassza ki a bejelentkezés dátumát");
                return;
            }

            isCheckInSelected = false;

            LocalDate minCheckOutDate = checkInDate.plusDays(1); // bejelentkezés utáni nap
            int year = minCheckOutDate.getYear();
            int month = minCheckOutDate.getMonthValue() - 1;
            int day = minCheckOutDate.getDayOfMonth();

            DatePickerFragment fragment = DatePickerFragment.newInstance(year, month, day);
            fragment.show(getSupportFragmentManager(), "checkOut");
        });
    }

    private void initSpinner(int resId, Spinner spinner) {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, resId,
                android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        int correctedMonth = month + 1; // hónap korrigálása a LocalDate miatt
        LocalDate selectedDate = LocalDate.of(year, correctedMonth, dayOfMonth);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String dateFormatted = selectedDate.format(formatter);

        if(isCheckInSelected) {
            checkInDate = selectedDate;
            checkInButton.setText(dateFormatted);
        } else {
            checkOutDate = selectedDate;
            checkOutButton.setText(dateFormatted);
        }
    }
}