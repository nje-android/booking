package hu.nje.booking;

import java.time.LocalDate;

public class Booking {
    private String destination;
    private LocalDate checkInDate;
    private LocalDate checkOutDate;
    private int adults;
    private int children;
    private int rooms;
    private boolean isBusiness;

    public Booking(String destination, LocalDate checkInDate, LocalDate checkOutDate, int adults, int children, int rooms, boolean isBusiness) {
        this.destination = destination;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        this.adults = adults;
        this.children = children;
        this.rooms = rooms;
        this.isBusiness = isBusiness;
    }

    public String getDestination() {
        return destination;
    }

    public LocalDate getCheckInDate() {
        return checkInDate;
    }

    public LocalDate getCheckOutDate() {
        return checkOutDate;
    }

    public int getAdults() {
        return adults;
    }

    public int getChildren() {
        return children;
    }

    public int getRooms() {
        return rooms;
    }

    public boolean isBusiness() {
        return isBusiness;
    }
}
