package hu.nje.booking;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class DatePickerFragment extends DialogFragment {

    private static final String ARG_YEAR = "year";
    private static final String ARG_MONTH = "month";
    private static final String ARG_DAY = "day";

    private int year;
    private int month;
    private int day;

    public DatePickerFragment() {}

    public static DatePickerFragment newInstance(int year, int month, int day) {
        DatePickerFragment fragment = new DatePickerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_YEAR, year);
        args.putInt(ARG_MONTH, month);
        args.putInt(ARG_DAY, day);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null) {
            year = args.getInt(ARG_YEAR);
            month = args.getInt(ARG_MONTH);
            day = args.getInt(ARG_DAY);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), (MainActivity)getActivity(),
                year, month, day);
        // múltbéli dátumok tiltása
        LocalDateTime dateTime = LocalDateTime.of(year, month+1, day, 0, 0);
        ZoneOffset systemZoneOffset = ZoneId.systemDefault().getRules().getOffset(Instant.now()); // eltolás
        Instant instant = dateTime.toInstant(systemZoneOffset); // fragment-nek átadott dátum Instant típussá alakítása
        dialog.getDatePicker().setMinDate(instant.toEpochMilli());
        return dialog;
    }
}
