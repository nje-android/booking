# Booking

## Bevezető

A Booking alkalmazás fejlesztésének célja a booking.com online szálláskereső portál keresőfelületének megvalósítása Android rendszerre. Az alkalmazás fejlesztése során megismerkedhetünk az alapvető vezérlőkkel és azok tulajdonságaival, illetve egy nagyon egyszerű vezérlőelrendezési módszerrel is. Megtanulhatjuk továbbá a felületi elemeket vezérlő események működését, vezérlőkből való információ kiolvasást és megjelenítést egyaránt.

Tetszőleges böngészővel nyissuk meg a booking.com kezdőoldalát és vizsgáljuk meg a keresést megvalósító űrlapot. Az oldalon megjelenő vezérlők közül az alábbiakat használjuk fel:

-	„Találjon rá következő szállására” – egyszerű szöveg
-	„Hová készül” – szövegbeviteli mező, ahová a célállomást kell megadni (pl.: Balaton)
-	Bejelentkezés dátuma – dátumkiválasztó mező, ahol az utazás kezdetét adjuk meg
-	Kijelentkezés dátuma – dátumkiválasztó mező, ahol az utazás végét adjuk meg
-	Felnőttek – felnőttek száma
-	Gyermekek – velünk utazó gyermekek száma
-	Szobák – keresett szállás szobáinak száma
-	„Üzleti útra megyek” – checkbox
-	Keresés gomb – kattintás eseményre összegyűjtjük az űrlapon beírt adatokat és elindítjuk a keresési funkciót a tárolt szállások között

A fent felsorolt vezérlőket fogjuk megjeleníteni egy egyoldalas Android alkalmazásban és a keresés gomb megnyomására pedig az adatokat összegyűjteni az űrlapról. Keresés és találatmegjelenítés nem része az alkalmazásnak.

## Projekt létrehozása

Android Studio-ban hozzunk létre egy Empty Views Activity típusú projektet, aminek a neve legyen Booking, csomagazonosítónak pedig hu.nje.booking -t adjuk meg. Programozási nyelvnek válasszuk a Java-t, Build configuration language legyen Groovy DSL (build.gradle). A többi beállítás maradjon az Android Studio által felajánlott alapértelemezett konfiguráció.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/515225e9a2f041ffa796d3c9c866bdc7cd26ff83)

## Felület

A létrejött projektben jelenleg egy felülettel rendelkező komponens (Activity) szerepel MainActivity névvel. Ehhez a komponenshez tartozik egy felhasználói felület activity_main.xml néven a res (resources, azaz erőforrások) mappa layout almappájában (bal oldalon a projekt fájljai között keresendő). A felülethez tartozó code behind Java kódfájlt java csomópont és alatta az általunk megadott csomagnév alatt MainActivity.java néven találjuk meg. Ezen kívül még a komponensek létrehozásakor történik egy regisztráció a manifest állományban is.

Nyissuk meg az activity_main.xml állományt. Az Android Studio rendelkezik beépített designer funkcióval, ahol tetszőleges bonyolultságú felületek hozhatók létre design time. A felületi tervezőnek három nézete elérhető:

-	Design nézet: felületi előnézet és blueprint nézet, vezérlő attribútumok, component tree a vizuális fa megjelenítéséhez, vezérlő eszköztár
-	Code nézet: xml megjelenítés
-	Split nézet: előnézet és xml nézet egymás mellett megjelenik

A felület létrehozását kezdjük először xml nézetben, cseréljük le a fájlban található gyökér xml tag-et (nyitó és zárót egyaránt) androidx.constraintlayout.widget.ConstraintLayout -ról LinearLayout -ra. Az így létrejött LinearLayout elrendezésen belül a kezdő tag-ben adjuk hozzá az android:orientation="vertical" attribútumot. Ezzel a művelettel lecseréltük az alapértelmezettként kapott elrendezési módszert (későbbiekben a ConstraintLayout-ot érdemes használni szinte mindenhol) egy jóval egyszerűbbre, ami a benne elhelyezett vezérlőket képes egymás alá vagy egymás mellé megjeleníteni attól függően, hogy az orientation attribútumot vertical-ra vagy horizontal-ra állítjuk be.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/a4247cf49e796a34f3e36dfc5194482578e4851e)

Térünk vissza design nézetbe, a Component Tree ablakban (bal oldalon alul) töröljük ki a bennmaradt TextView vezérlőt, így teljesen üres felületről indulhatunk.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/2b052c877b3fa845bde244a7914ab7bb6b53205f)

Palette eszköztárból (bal oldalon felül) keressük ki és sorrendben egymás utána adjuk hozzá az alábbi vezérlőket:
-	TextView (Common kategória): egyszerű szöveg megjelenítésért felelős vezérlő, ami a -	„Találjon rá következő szállására” szöveget jeleníti meg a felületen
-	Plain Text (Text kategória, vezérlő típusa egyébként EditText): „Hová készül” szövegbeviteli mező, ahová a felhasználó begépeli az utazási célját
-	Button (Common kategória): dátumkiválasztó komponens fog megjelenni, ha a felhasználó megérinti a gombot, így be tudja állítani a bejelentkezés dátumát
-	Button: dátumkiválasztó komponens fog megjelenni, ha a felhasználó megérinti a gombot, így be tudja állítani a kijelentkezés dátumát
-	TextView: „Felnőttek száma” statikus szöveg megjelenítése 
-	Spinner (Containers kategória): lenyíló lista, amiben a felnőttek száma választható ki
-	TextView: „Gyermekek száma” statikus szöveg megjelenítése 
-	Spinner: lenyíló lista, amiben a gyermekek száma választható ki
-	TextView: „Szobák száma” statikus szöveg megjelenítése 
-	Spinner: lenyíló lista, ahol a szobák száma választható ki
-	CheckBox (Buttons kategória): „Üzleti útra megyek”
-	Button: Keresés gomb

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/a803d9353554a8239d200c599ffb9d8266594671)

### Azonosító (id)

Az így elkészül felületen először állítsuk be minden vezérlőnek az azonosítóját. Azonosítók segítségével tudjuk meg a későbbiekben a Java kódban, illetve a felületen melyik vezérlővel dolgozunk. Tételezzük fel, hogy egy felületi megjelenítésben használunk 100 db nyomógombot és meghagyjuk a rendszer által generált button1, button2, stb. azonosítókat. Könnyen belátható, hogy néhány nyomógomb használata után már fogalmunk se lesz melyik vezérlővel dolgozunk a felületen, így értelemszerűen minden programkódban is használt vezérlőnek fogunk adni egy egyedi azonosítót. Olyan vezérlőknél, amik csak statikus tartalmat jelenítenek meg (feliratok, képek, stb.) nem kötelező az alapértelmezett azonosítók módosítása.

Ha kiválasztunk egy vezérlőt a felületen vagy a Component Tree-ben, a jobb oldalon lévő Attributes ablak a vezérlő tulajdonságai jelennek meg. Attributes ablak keresőmezőjében tudunk az attribútumok között könnyebben keresni, viszont az id (azonosító) attribútum az első elem lesz. Válasszuk ki a vezérlőket egyesével és állítsuk be az alábbi azonosítókat a fent leír vezérlősorrend alapján:

-	headerTextView
-	destinationEditText
-	checkInButton
-	checkOutButton
-	adultsTextView
-	adultsSpinner
-	childrenTextView
-	childrenSpinner
-	roomsTextView
-	roomsSpinner
-	businessCheckBox
-	searchButton

Jól látható, hogy az azonosítók tartalmazzák a vezérlő rendeltetését és típusát is. Az azonosítókban számít a kis-nagy betű eltérés, erre érdemes nagyon odafigyelni.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/019228c349a9a3478c548f6ee78ff9aadef70944)

### Margin
Vezérlők pozícionálását LinearLayout-ban csak a szülő elemhez (tehát a LinearLayout-hoz) valósíthatjuk meg. Margin attribútummal érhetjük el ezt a viselkedést. Válasszuk ki az első vezérlőt és keressünk az attribútumok között a „margin” szóra és bontsuk ki a megjelenő layout_margin-t. Margin-t külön és egybe is beállíthatunk, minden értéket dp (sűrűségfüggetlen pixel) mértékegységgel kell ellátni.

Legfelső TextView margin értékei:

-	layout_marginStart: 8dp
-	layout_marginEnd: 8dp
-	layout_marginTop: 16dp

Margin-okat állítsuk be a többi vezérlőre is tetszés szerint.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/9fa302b5bb50d80f4679b5ad036af27e9a7a6da3)

### Text és hint

Android-ban a vezérlők szövegmegjelenítését a text attribútummal állíthatjuk be mind a TextView, EditText és Button vezérlőknél. Állítsuk be a szövegeket a felületünkön a booking.com-on látható szövegezés alapján. Szövegméretet a textSize attribútummal, a szöveg pozícióját pedig gravity-vel módosíthatjuk.

destinationEditText vezérlőnél töröljük ki a text értékét és állítsunk be egy placeholder szöveget. Android-on ezt a hint attribútummal valósíthatjuk meg.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/6afb980c58cbb556dad0af4fe371741e2cb2a96a)

### Felületi hibák

Designer nézetben jobb felül, illetve a Component Tree-ben egyes vezérlők mellett azt tapasztalhatjuk, hogy megjelent (már a vezérlő hozzáadásakor) egy piros felkiáltójel, ami tulajdonképpen felületen található hibákra figyelmeztet mindet. Ha a designer jobb felső sarkában lévőt választjuk ki a teljes felületre vonatkozó összes hibát látni fogjuk egy új ablakban. Láthatjuk, hogy az EditText és a Spinner vezérlőkre panaszkodik, egészen pontosan „Touch target size too small” üzeneteket láthatunk, tehát a vezérlőnk mérete túl kicsi, nem érintésvezérlésre optimalizált. Gyors megoldás a problémára ha a hibára jobbal kattintunk, majd „Show Quick Fixes” és válasszuk az „Set this item’s android:minHeight to 48dp” lehetőséget. Végezzük el ugyanezt a többi vezérlőre is, ahol ez a hiba előjött.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/9a985e655a77377aacd374f17407de69818d18d0)

Ezen a ponton a felületünk elkészült, a továbbiakban a Java kódban fogunk tevékenykedni.

# MainActivity

## Vezérlők inicializálása

Felületen elhelyezett vezérlőkhöz hozzunk létre adattagokat a felülethez (activity_main.xml) tartozó kódfájlban (MainActicity.java). Minden a designer-ben használt vezérlőtípushoz léteznek Java osztályok, őket kell példányosítani a Java kódban való használatukhoz. A Booking alkalmazásban csak azokat a vezérlőket fogjuk itt példányosítani, ahonnan adat olvasunk ki vagy írunk be, illetve amelyik vezérlőnek egy eseményére szeretnénk feliratkozni. Érdemes változónévnek ugyanazokat az elnevezéseket választani, amit a felületen is beállítottunk azonosítóként.

Objektumorientált programozásban megszokottaktól eltérően Android-ban a vezérlőket nem a konstruktorban inicializáljuk, hanem az onCreate életciklusfüggvényben. onCreate-ben is egészen pontosan a setContentView utasítás után, mivel ezen a ponton érhetők majd el a vezérlők. Vezérlők inicializálását az Activity ősosztálytól kapott findViewById függvénnyel valósítjuk meg. A függvény nevében lévő View az összes vezérlő őstípusa, a ById részt pedig a korábban tárgyaltak alapján már könnyű kitalálni. A findViewById bejövő paraméterként egy integer azonosítót vár, ami az R.id.<felületen lévő vezérlő azonosítója> kifejezéssel érünk el. A R (resources rövidítése) típus a rendszer által generált típus, ahogy a felületen felteszünk egy vezérlőt és azonosítót adunk neki, itt egy konstans jön létre. Módosítani az R osztályt nem lehet, a rendszer feladata tartalmának frissítése. Tulajdonképpen az erőforrásaink (resources) és a Java kódunk között az R osztály segítségével tudunk kapcsolatot tartani. Ugyanígy, ha egy képfájlt elhelyezünk a drawable mappába pl.: wallpaper.jpg névvel, az R.drawable.wallpaper azonosítóval tudunk rá hivatkozni a Java kódban.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/a43d8f428744583dbefbcd33e3bd5ef09208d209)

## Eseménykezelés

Keresés gombot készítsük fel arra az esetre, ha hozzáér a felhasználó valamilyen utasításblokk fusson le. Android-ban a nyomógombnak kattintás eseménye van (mivel egérrel is használható a rendszer), így a nyomógomb megnevezésével és a setOnClickListener függvényhívással iratkozunk fel erre az eseményre. A setOnClickListener bemenetként egy OnClickListener interface implementációt vár. Ahelyett, hogy létrehoznánk egy osztályt, ami örököl ettől az interface-től és példányosítanánk a MainActivity-ben a legegyszerűbb megoldás ha lokálisan implementáljuk az interface függvényeit. Szerencsénkre a fejlesztőkörnyezet segít, ha begépeltük az setOnClickListener függvényt és a kezdő zárójelet írjuk be, hogy new, majd egy szóköz után felajánlja a View.OnClickListener megvalósítását egy context ablakban (Ctrl+Space ha esetleg etűnne). A generált kódból jól látható, hogy egyetlen onClick függvénye volt az interface-nek, bemenete egy View típus, ami gyakorlatilag az a vezérlő, akire rákattintottak. onClick függvényblokkjába kell elhelyezni azt az utasítássorozatot, amit a kattintásra szeretnénk futtatni.

Ide helyezzünk el egy Toast felugró üzenetet, majd futtassuk emulátorban/fizikai eszközön az alkalmazást:

```java
Toast.makeText(getApplicationContext(), "Keresés kattintás", Toast.LENGTH_SHORT).show();
```

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/0649baac67bf3a2a376f034aa20d2d0bc7f1f18b)

Az előbbi kifejezést jóval tömörebben is megfogalmazhatjuk egy lambda kifejezéssel (mivel az interface egy függvényt tartalmaz): v -> {}. Lambda operátor bal oldalán a bemeneti paraméterek vannak felsorolva (View típusú v változó, de itt még a típust sem kell kiírni), jobb oldalán pedig az utasításblokk.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/d9d52c9d689303a969bedb0a9080ca6cb1f9a5db)

## Spinner
A felületre felhelyezett három Spinner vezérlőt fogjuk következőkben működésre bírni. Első lépésként meghatározzuk a három lenyíló lista adatforrását. Esetünkben a Spinner vezérlők mindig egy meghatározott, statikus értékhalmazt fognak megjeleníteni, ebből választhat ki a felhasználó egy értéket. Android rendszerben ilyenkor érdemes erőforrásként tárolni a megjelenítendő adatokat, ezt megtehetjük string tömbök létrehozásával a strings.xml állományban. A Spinner-nek az erőforrásként definiált tömböket programozott módon tudjuk hozzárendelni, illetve a lenyíló lista megjelenítését befolyásolni.

### Erőforrás tömbök

Hozzunk létre három tömböt a res/values/strings.xml állományban, minden Spinner-hez saját adatforrás fog tartozni.

```xml
<string-array name="number_of_adults">
    <item>1</item>
    <item>2</item>
    <item>3</item>
    <item>4</item>
    <item>5</item>
</string-array>

<string-array name="number_of_children">
    <item>0</item>
    <item>1</item>
    <item>2</item>
    <item>3</item>
</string-array>

<string-array name="number_of_rooms">
    <item>1</item>
    <item>2</item>
    <item>3</item>
    <item>4</item>
</string-array>
```

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/f80020f8389062a4455672727b5300f8e11b8b08)

### Spinner megjelenítés

Spinner vezérlő Android-on egy lista vezérlőnek felel meg. Minden listamegjelenítéshez szükség van az adatforrás és a vezérlőelemen túl egy Adapter-re is. Az Adapter felelősége, hogy kezelje az adathalmazt és a listaelemek megjelenítését szabályozza. Esetünkben az adatforrás egy tömb, amihez már a rendszerben létezik egy Adapter típus ArrayAdapter<T> néven. Az ArrayAdapter<T> képes tömböket és indexelhető listákat megjeleníteni, paraméter típusában egy megjelenítendő elem típusa kerül (pl. szöveg esetén CharSequence, String, stb.). ArrayAdapter-nek a createFromResource függvényét fogjuk használni az objektum létrehozásához, mivel az adatforrásunk erőforrások közé lett létrehozva. A függvénye első paramétere egy speciális típus Android-ban a Context. Mivel a Context típustól örököl az Activity ősosztály is ezért az első paraméter this lesz (alternatív megoldás, hogy a Context objektumot a getApplicationContext() függvénnyel kérjük el). Második paramétere az adatforrás, esetünkben pl. R.array.number_of_adults. Harmadik paramétere a kiválasztott listaelem megjelenítéshez tartozó layout. Ezt a felületi elemet mi is létre tudjuk hozni (layout mappába létrehozunk egy xml-t és saját felületet állítunk össze), de használhatjuk az Android rendszerrel érkező felületi elemeket is. Ezért használjuk az android.R.layou_simple_spinner_item layout-ot, aminek ha megnézzük a definícióját tulajdonképpen csak egy TextView vezérlőt tartalmaz. Adapter objektumnak még egy felületi megjelenítést kell beállítani, amikor lenyitjuk a Spinner-t a megjelenő listaelemeknek is külön layout-ot rendelünk (setDropDownResource függvényhívás). Végül a Spinner vezérlőhöz hozzárendeljük a felparaméterezett Adapter objektum referenciáját.

A megoldásban létrehoztunk három segédfüggvényt, ami felparaméterezi a Spinner vezérlőket. Illetve a segédfüggvényeket meghívtuk az onCreate függvényben.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/ccf7fc340e89456ef4dd0390f26848e034309670)

### Spinner refactor

Az előző megoldáson érdemes optimalizálni, mivel túl sok kódrészletet ismételtünk meg. Törekedjünk arra, hogy tömörebben fogalmazzuk meg a Spinner-ek inicializálását. Ha megvizsgáljuk a három segédfüggvényt észrevehetjük, hogy csak két különbség található bennük. Az egyik az adatforrás, a másik pedig a Spinner vezérlő. Az erőforrásra hivatkozást egy integer konstanssal oldja meg az Android, tehát készíthetünk egy függvény, ami az erőforrás integer azonosítóját várja bemenő paraméterként. Tulajdonképpen ugyanez a gondolatot vezethetjük tovább a vezérlőknél, a függvény második paraméterként várhatja a Spinner referenciáját. Így a három függvényt eggyel tudjuk helyettesíteni és ha egy új Spinner-t helyeznénk fel a felületre a Java kódban csak egy utasítással (az új segédfüggvény hívásával) kellene az kiegészíteni.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/c9f6b278d3cb4b9efb620622799d0b4623d8a570)

## Dátumkiválasztás
### Dátumkiválasztó felület

Bejelentkezés és kijelentkezés gombok megnyomására szeretnénk egy-egy dátumkiválasztó felületet megjeleníteni. Mivel a dátumkiválasztás az eredeti felületre nem fér el ezért felugró ablakként lesz látható. Ehhez létre fogunk hozni egy Fragment komponenst, ami az Activity-hez hasonlóan egy felülettel és code-behind állománnyal rendelkező komponens. A Fragment-nek viszont nem kell elfoglalni teljesen a képernyőt, egy Activity felületen belül lehet akár több Fragment is egy időben. Fragment-eknek is több típusa létezik, mi egy dialógus ablakként megjelenő DialogFragment leszármazottat fogjuk használni.

Hozzunk létre egy osztályt a DatePickerFragment néven, örököljön az androidx.fragment.app.DialogFragment ősosztálytól. DatePickerFragment típusból több létezik Android-ban, mi az androidx csomagban található verziót használjuk. androidx csomagokban lévő típusok visszafelé is kompatibilisek, ezt a csomagot korábban support-nak hívták, viszont a Google néhány évvel ezelőtt átnevezte és újragondolta a support library működését. Az osztályon belül felül kell definiálni az onCreateDialog függvényt (Android Studio-ban Ctrl + O felugró menüből kiválasztható) a dialógusablak megjelenítéséért. A dátumkiválasztó felparaméterezéséhez lekérdezzük a mai dátumot, ehhez a Java 8-ban megjelent java.time csomag típusait használjuk, esetünkben a LocalDate típust. Amikor használatba szeretnénk venni a LocalDate-et a fejlesztőkörnyezet figyelmeztetni fog, hogy egy meghatározott Android API szint fölött elérhető. Két lehetőségünk van, megemeljük a minimális API szintet a gradle állományban vagy készítünk egy elágazást (if-else), amiben figyeljük milyen Android rendszeren fut az alkalmazásunk. Ha a 26-os (Android Oreo) szintnél régebbi operációs rendszeren használjuk, akkor pl. LocalDate helyett választhatjuk a Calendar típust, ha újabb akkor pedig a LocalDate lesz a megoldás a dátumkezelésre. Válasszuk az első lehetőséget, így nem kell felesleges elágazásokkal terhelni a kódbázist, továbbá egy modern dátum és időkezelést használhatunk. A dátum kiválasztónál paraméterként adhatjuk meg az évet, hónapot és napot, egyetlen probléma hogy a hónapok értékét 0-11 közötti értékkel kell megadni. Ez a viselkedés még a Calendar típusból maradt, ahol a hónap szintén 0-11 értéket vehetett fel. DatePickerDialog típus példányosításánál első paraméter a Context objektum (itt használhatjuk a getActivity() függvényt), második paraméter pedig egy olyan típus, ami megvalósítja az OnDateSetListener interface-t. Erre azért van szükség, mert kell egy olyan komponens, ahol meg szeretnénk kapni a kiválasztott dátumot. Mivel minden más adat/vezérlő a MainActivity-ben található ezért a MainActivity fogja implementálni az interface-t, a dátumkiválasztó csak megjeleníti a felületet, a felhasználó által kiválasztott dátumra viszont a MainActivity-ben lesz szükségünk. Könnyen belátható, hogy ez a megszorítás számunkra jól jön, mert nem kell külön megoldani az Activity és Fragment között adatátadást. A konstruktor utolsó három paramétere pedig az év, hónap (korrigált érték) és nap lesz.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/b637c7632f8c6af1517abcc1e6f48000f4ce7de8)

### Dátumkiválasztó megjelenítése

A Fragment hozzáadása és az onCreateDialog implementálása után még fordítási hibát kapunk. Ennek oka, hogy a MainActivity még nem valósítja meg OnDateSetListener interface-t. Valósítsuk meg az interface implementációját és adjuk hozzá az onDateSet függvényt. Bemenő parméterként megkapjuk a kiválasztott dátumot (év, hónap, nap külön), ami jelenítsünk meg a bejelentkezés gomb felületén. Formázott megjelenítéshez alakítsuk vissza a dátumot LocalDate típussá és használjuk a DateTimeFormattel típust. A nyomógomb felületére a setText függvénnyel írhatunk.

Utolsó lépésként jelenítsük meg a dátumkiválasztót a bejelentkezés kattintására. Iratkozzuk fel az eseményre, majd hozzunk létre egy példányt a DatePickerFragment-ből. Mivel DialogFragment típusú a komponensünk, ezért a show függvénnyel tudjuk megjeleníteni. Első paramétere egy FragmentManager típus, ennek feladata a Fragment objektumok megjelenítése és eltűntetése a felületről. Mivel a saját Fragment-ünk androix csomagból származik, ami régen a support library volt ezért az Activity-n belül elkérhetjük az aktuális FragmentManager példányt a getSupportFragmentManager() függvényhívással. Második paramétere egy opcionális tag, itt adjuk meg a "checkIn" szöveget.

Futtassuk az alkalmazást és ellenőrizzük a dátumvezérlő megjelenítését és a kiválasztott dátum kiírását a gomb felületére.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/852b87c007ffafb5f457d039ef12443fb18e9b41)

### Kijelentkezés

Kijelentkezés dátumkiválasztónak ugyanazt a típust fogjuk használni mint a bejelentkezésnél, amivel egy probléma van, ugyanabban az onDateSet függvényben kapjuk meg a kiválasztott dátumot. Hogyan tudnánk a két dátumkiválasztót egymástól megkülönböztetni? Egy lehetséges megoldás, ha felveszünk egy változót (esetünkben egy boolean-t), aminek értéket adunk a bejelentkezés vagy kijelentkezés megjelenítésénél és ez alapján el tudjuk dönteni az onDateSet-ben kitől jött az eredeti kérés.

Futtassuk az alkalmazást és ellenőrizzük a bejelentkezést és a kijelentkezést.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/302ae831177a61040c08de9e769d88bb49f525d4)

### Kezdődátum átadása

Az alkalmazás jelenlegi lehetővé teszi két dátum kiválasztását, mindkét dátum kiválasztásánál a dátumkiválasztóban megjelölt dátum a mai dátum lesz. Ez a viselkedést szeretnénk úgy módosítani, hogy a dátumkiválasztó Fragment paraméterként megkapja a megjelenítendő dátumot, valamint egy másik igényünk pedig a múltbéli dátumok letiltása lesz. A Fragment paraméterátadás némileg trükkös, nem az objektumorientálságban megszokott konstruktoron keresztül fogjuk megvalósítani. Mobil operációs rendszereknél a komponensek életciklusára is fel kell készülnünk, ezért az adatátadáskor az életciklus függvényeket kell használni. A Fragment-ünk három paramétert fog várni, ehhez hozzunk létre három konstans értéket mivel a paraméterátadásnál nevesíteni kell azok neveit, kétszer pedig ugyanazt a string-et nem szeretnénk begépelni (pontosabban az elgépelést szeretnénk elkerülni). Adattagként az évet, hónapot és napot vegyük fel változóként, értéküket az onCreate függvényben  (Ctrl + O) fogják megkapni argumentumokból kiolvasva. Fragment-ekhez hozzá tudunk rendelni argumentumokat kulcs-érték formájában. Ezt a kulcs-érték tárolót lehet elérni a getArguments() függvénnyel, a visszaadott típus pedig egy Bundle (kulcs-érték tároló típus Android-ban) lesz. Amennyiben tartalmaz értékeket akkor kiolvassuk és elmentjük az adattagokba.

Végül hozzunk létre egy osztályhoz tartozó (statikus) függvényt, ami legyártja a DatePickerFragment objektumot. Bemenő paraméterként várja az alapértelmezettként megjelölt évet, hónapot és napot. Üresen létrehozunk egy DatePickerFragment példányt (ezért kell a paraméter nélküli DatePickerFragment konstruktor), majd rendeljük hozzá a setArguments függvénnyel a felparaméterezett Bundle-et. MainActivity-ből most már a newInstance metódust fogjuk használni DatePickerFragment objektum példányosításához.

Múltbéli dátumok tiltását egy utasítással megoldhatjuk, a setMinDate-nek milliszekundumban kell átadni a kezdődátumot. Ennek hatására minden más korábbi dátum le lesz tiltva a vezérlőben.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/7b0a6d0d546c3b44096570ad98c9dab66b52bebc)

### Dátumok ellenőrzése

MainActivity-ben vegyünk fel két adattagot a bejelentkezéshez és a kijelentkezéshez, mindkettő legyen LocalDate típusú. Bejelentkezésnél kérdezzük le a mai dátumot és használjuk az előbb létrehozott newInstance függvényt. Kijelentkezés kiválasztásánál először ellenőrizzük, hogy a bejelentkezés dátumát már megadták-e. Erre azért van szükség, mert a kijelentkezés dátumválasztásánál az alapértelmezett dátumnak a bejelentkezés + 1 napot szeretnénk látni. Napok hozzáadását a plusDays függvénnyel érjük el, így egy új LocalDate típust kapunk.

Minimális módosításra szükségünk van a DatePickerFragment osztályban is. A System.currentTimeMillis() kifejezés a mai dátumot adja vissza milliszekundumban, viszont nekünk az átadott dátumból kell ugyanezt kiszámolni. Ehhez felhasználjuk a LocalDateTime, ZoneOffset és Instant típusokat.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/41afe2eb993031ba3221364fc91aec9a908382c9)

## Adatok összegyűjtése

Keresés kattintás eseményben olvassuk ki a vezérlők tartalmát. EditText vezérlőben a text attribútum értéket a getText függvénnyel olvashatjuk ki. A Spinner vezérlőknél a kiválasztott elemet a getSelectedItem adja vissza, viszont a visszaadott object típust először szöveggé majd egész számmá konvertáljuk. CheckBox isChecked függvényével olvashatjuk ki, hogy a felhasználó bejelölte vagy sem.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/0419fccd6f0bf9d17c68ff9861afb67c88b533ee)

### Modell létrehozása

Keresési adatok tárolásához hozzunk létre egy típust, ami reprezentálja az egész űrlap tartalmát. A Booking osztály tárolja az úti célt, a két dátumot, felnőttek-gyermekek-szobák számát, illetve hogy üzleti útra keresünk.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/39fd8633a1136974bf37604c91c36a006220f7c4)

### Keresések tárolása

Hozzunk létre egy listát, amiben a Booking objektumokat tároljuk, valamint a felületre helyezzünk fel egy vezérlőt, ami megjeleníti a lista elemszámát (keresések/foglalások számát).

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/bd45c36a652317c7387bdc813c48073b7bbde254)

### Ellenőrzések

Végül oldjuk meg, hogy az űrlapon ellenőrzött adatbevitel valósuljon meg. Minden hibaüzenetet Toast üzenetben írunk ki a felhasználónak, ezért érdemes egy segédfüggvényt (showMessage) készíteni, amiben a Toast létrehozását és megjelenítését végrehajtjuk. Ellenőrizzük le, hogy az úti cél és a dátumok kitöltésre kerültek, valamint hasonlítsuk össze a ki- és bejelentkezés dátumát. A kijelentkezésnek legalább egy nappal később kell lennie, mint a bejelentkezésnek, ellenkező esetben hibaüzenetet jelenítünk meg.

[Gitlab projekt link](https://gitlab.com/nje-android/booking/-/commit/4c7df74e80186ca95d020fdf8e2ce7f71de9b1e3)
